import React from 'react';
import Favorites from './components/Favorites';
import Basket from './components/Basket';
import ProductList from './components/ProductList';
import './components/style.scss'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  // Link,
  NavLink
} from "react-router-dom";

function App() {
  
  return (
    <Router>
        <div className={'app'}>
          {/* <ModalEnter/> */}

        <header className="header">
          <NavLink to="/" exact>Home</NavLink>
          <NavLink to="/fav">Favorites</NavLink>
          <NavLink to="/basket">Basket</NavLink>
        </header>

        <Switch>
          <Route path="/fav">
            <Favorites />
          </Route>

          <Route path="/basket">
            <Basket />
          </Route>

          <Route path="/">
            <ProductList/>
          </Route>

        </Switch>
        </div>
    </Router>
  );
}

export default App;

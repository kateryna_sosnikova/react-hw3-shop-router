import React from 'react';
import { useState } from 'react';
import Modal from './modal/Modal'
import Button from '../button/button'

const Container = () => {

        const [firstModal, setFirstModal] = useState(false);
        const [secondModal, setSecondModal] = useState(false);

        const openFirstModal = () => setFirstModal(true);
        const openSecondModal = () => setSecondModal(true);

        const closeFirstModal = () => setFirstModal(false);
        const closeSecondModal = () => setSecondModal(false);

        const firstButtonProps = {
            onClick() {openFirstModal()},
            backgroundColor: 'green',
            text: 'Open first modal'
        }

        const secondButtonProps = {
            onClick() {openSecondModal()},
            backgroundColor: 'red',
            text: 'Open second modal'
        }

        const firstModalProps = {
            header: "Do you want to delete this file?",
            text: "Once you delete this file, it won't be possible to undo this action. Are you sure to continue?", 
            closeButton: true,
            onClick: closeFirstModal,
            actions: [<Button text="OK" onClick={closeFirstModal}/>, 
                    <Button text="Cancel" onClick={closeFirstModal}/>]
        }

        const secondModalProps = {
            header: "Are you ready to become a developer?",
            text: "Well Now You're Here, There's No Way Back", 
            onClick: closeSecondModal,
            closeButton: false,
            actions: [<Button text="Sure" onClick={closeSecondModal}/>, 
                        <Button text="Bye" onClick={closeSecondModal}/>]
        }
        console.log(firstButtonProps);
        return (
            <>
                <Button {...firstButtonProps} />
                {firstModal && < Modal {...firstModalProps}/>}

                <Button {...secondButtonProps}/>
                {secondModal && <Modal {...secondModalProps} />}
            </>
        )
    };

    export default Container;





    
// class ModalEnter extends React.Component {

//     render() {
//         return (
//             <React.Fragment>
//                 < Modal header = "Do you want to delete this file?" text = "Once you delete this file, it won't be possible 
//                 to undo this action.Are you sure to continue?" textBtn="Open First Modal" colorBtn="red" 
//                 closeButton = "true" actions = { [<Button text="OK" />, <Button text="Cancel" />]} />

//               <Modal header="Are you ready to become a developer?" text="Well Now You're Here, There's No Way Back" textBtn="Open Second Modal" colorBtn="green"
//               closeButton="true" actions={[<Button text="Sure" />, <Button text="Bye" />]} />
//           </React.Fragment>
//         )}
// }

// export default ModalEnter;
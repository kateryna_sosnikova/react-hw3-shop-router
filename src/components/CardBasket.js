import React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';

import Button from './Button'
import Modal from './Modal'
import { FaTrash } from "react-icons/fa";

const CardBasket = ({ name, price, article, url, color, addBasket, removeFromBasket, id, card }) => {

    const [modalState, setModalState] = useState(false);

    const modalContent = {
        text: "Вы действительно хотите удалить товар из корзины?",
        header: "Товар будет удален безвозвратно",
        onClick: () => setModalState(false),
        actions: [<Button text="ОК" onClick={() => { removeFromBasket(card); setModalState(false) }} />,
        <Button text="Нет, хочу оставить" onClick={() => setModalState(false)} />]
    }

    return (
        <li className="card-item" id={id}>
            <img className="card-image" src={url} alt='item-pic' />
            <span onClick={() => setModalState(true)}><FaTrash /></span>
            {modalState && <Modal {...modalContent} />}
            <h2 className="card-name">{name}</h2>
            <p className="card-article">Article: {article}</p>
            <p className="card-color">Color: {color}</p>
            <p className="card-price">Price: {price}</p>
        </li>

    )
}

export default CardBasket;

CardBasket.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    article: PropTypes.string,
    url: PropTypes.string,
    color: PropTypes.string,
    id: PropTypes.number,
    onClick: PropTypes.func
}

CardBasket.defaultProps = {
    price: '$9.99'
}
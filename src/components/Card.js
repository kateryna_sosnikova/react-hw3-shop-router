import React from 'react';
import {useState} from 'react';
import PropTypes from 'prop-types';

import Button from './Button'
import Modal from './Modal'
import { FaRegStar } from "react-icons/fa";
import { FaStar } from "react-icons/fa";


const Card = ({name, price, article, url, color, active, addToFav, 
    removeFav, addBasket, id, card}) => {

    const [icon, setIcon] = useState(active);
    const [modalState, setModalState] = useState(false);

    const btnContent = {
        text: "Add To Card",
        onClick: () => setModalState(true)
    }

    const modalContent = {
        text: "Вы уверены, что хотите добавить товар в корзину?",
        header: "Товар будет добавлен в корзину",
        onClick: () => setModalState(false),
        actions: [<Button text="ОК" onClick={() => {addBasket(card); setModalState(false)}}/>,
        <Button text="Нет, не нужно" onClick={() => setModalState(false)}/>]
    }

    return (
        <li className="card-item" id={id}>
            <img className="card-image" src={url} alt='item-pic'/>
                { icon && 
                <span onClick={() => {addToFav(card); setIcon(false)} }><FaRegStar /></span>}        
                { !icon && <span onClick={() => {removeFav(card); setIcon(true)}}><FaStar /></span>}          
        
            <h2 className="card-name">{name}</h2>
            <p className="card-article">Article: {article}</p>
            <p className="card-color">Color: {color}</p>
            <p className="card-price">Price: {price}</p>
            <Button {...btnContent}/>
            { modalState && 
            <Modal {...modalContent}/> }
        </li>
       
    )
}

export default Card;

Card.propTypes = {
        name: PropTypes.string,
        price: PropTypes.string,
        article: PropTypes.string,
        url: PropTypes.string,
        color: PropTypes.string,
        id: PropTypes.number
    }

Card.defaultProps = {
    price: '$9.99'
}
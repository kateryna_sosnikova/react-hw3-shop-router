import React, { useState, useEffect } from 'react';
import Card from './Card'

function ProductList() {

    const [list, setList] = useState([]);
    const [fav, setFav] = useState([]);
    const [basket, setBasket] = useState([]);

    useEffect(() => {
        fetch('./product.json')
            .then(res => res.json())
            .then(data => setList(data))
    }, [])

    useEffect(() => 
    { if (fav.length > 0)
        { localStorage.setItem('Favorites', JSON.stringify(fav)) }},
        [fav])

    const addToFav = (card) => {
        const newCard = card;
        newCard.active = false;
        setFav(favIds => [...favIds, newCard]);      
    }

    const removeFav = (card) => {
        const allFav = localStorage.getItem('Favorites');
        const allFavFilters = JSON.parse(allFav);
        //console.log('allFavFilters', allFavFilters)
        setFav(allFavFilters.filter(item => item.id !== card.id));
    }


    useEffect(() => {
    if (basket.length > 0)
    { localStorage.setItem('Basket', JSON.stringify(basket)) }},
        [basket])

    const addBasket = (card) => {
        setBasket(idsInBasket => [...idsInBasket, card]);
    }

    return (
        <>
            <ul className='cards-container'>
                {
                    list.map(item =>
                        <Card
                            {...item}
                            key={item.id}
                            addToFav={addToFav}
                            removeFav={removeFav}
                            addBasket={addBasket}
                            card={item}
                        />
                    )}
            </ul>
        </>
    )

}

export default ProductList;
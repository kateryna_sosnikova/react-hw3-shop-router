import React from 'react';
import PropTypes from 'prop-types';

const Button = ({backgroundColor, text, onClick}) => {
    
    return(
        <button onClick={onClick} style={{ background: backgroundColor }}>
                            {text}</button>
    );
}

export default Button;

Button.propTypes = {
    background: PropTypes.string,
    text: PropTypes.string, 
    onClick: PropTypes.func
}

Button.defaultProps = {
    background: "grey",
    text: 'ОКЕЙ'
}


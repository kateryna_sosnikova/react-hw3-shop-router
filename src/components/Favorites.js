import React, { useState, useEffect } from 'react';
import Card from './Card'

const Favorites = () => {

    const [fav, setFav] = useState([]);

    useEffect(() => {
        const allFav = localStorage.getItem('Favorites');
        setFav(JSON.parse(allFav));
    }, []);

    useEffect(() => {
        localStorage.setItem('Favorites', JSON.stringify(fav));
    }, [fav])

    const removeFromFav = (card) => {
        setFav(fav.filter(item => item.id !== card.id));
    }

    //Basket adding from button
    const [basket, setBasket] = useState([]);

    useEffect(() => {
        const allBasketItems = localStorage.getItem('Basket');
        setBasket(JSON.parse(allBasketItems));
    }, []);

    useEffect(() => {
        localStorage.setItem('Basket', JSON.stringify(basket))
    }, [basket])

    const addBasket = (card) => {
        setBasket(idsInBasket => [...idsInBasket, card]);
    }

    const cardsFav = fav.map(item => 
        <Card 
            key={item.id} 
            card={item}
            addBasket={addBasket}
            removeFav={removeFromFav}
            {...item} />);

    return (
        <ul className="fav-container">
            {cardsFav}
        </ul>
    )
}

export default Favorites;




import React, { useState, useEffect } from 'react';
//import Modal from './Modal';
import CardBasket from './CardBasket';

const Basket = () => {

    const [data, setData] = useState([]);

    useEffect(() => {
        const allBasketItems = localStorage.getItem('Basket');
        setData(JSON.parse(allBasketItems));
    }, []);

    useEffect(() => {
        localStorage.setItem('Basket', JSON.stringify(data));
    }, [data])


    const removeFromBasket = (card) => {
        setData(data.filter(item => item.id !== card.id));
    }

    const itemsBasket = data.map(item => <CardBasket
        key={item.id}
        removeFromBasket={removeFromBasket}
        card={item}
        {...item} />);

    return (
            <ul className="basket-container">
                {itemsBasket}
            </ul>
    )

}

export default Basket;